class media::vars {
    $music     = lookup("media_subsonic")
    $rdomain   = lookup("root_domain")
    $sickbeard = lookup("media_sickbeard")
    $web_root  = lookup("media_web_root")
}
