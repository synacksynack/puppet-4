class media::service {
    common::define::service {
	"plexmediaserver":
	    ensure => running;
    }
}
