class trezorbridge::vars {
    $download = lookup("download_cmd")
    $nologin  = lookup("generic_nologin")
    $srvdir   = lookup("service_dir")
}
